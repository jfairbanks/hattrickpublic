//+------------------------------------------------------------------------+
//|                                     GoldTrailingStop.mq4, Version 1.00 
//|                      Copyright 2020, John Fairbanks (Apiary: ScrubFree) 
//|                                                                          
//|     This software is for use with the Gold Method Ichimoku trading group 
//|                                    https://www.tradingview.com/u/AGold54 
//|                                                                        
//+------------------------------------------------------------------------+
// 
#property copyright "This software is for use with the Gold Method Ichimoku trading group"
#property link      "https://www.tradingview.com/u/AGold54"
#property version   "1.00"
#property strict

// -----------------------------------------------------------------------------------------------------------------
// EA Settings ------
extern double StopToFlatPips         = 25.0;  // Min profit at which we move STF and start trailing
extern double TrailingStopBufferPips = 5.0;   // Number of "buffer" pips above or below the previous candles high/low when trailing

// -----------------------------------------------------------------------------------------------------------------
// EA Logic --------- 
void OnNewBar()
{
   int ordersFound = 0; int stfCount = 0; int trailingCount = 0;
   for (int i = OrdersTotal() - 1; i >= 0; i--)
   {
   	if (OrderSelect(i, SELECT_BY_POS) && OrderCloseTime() == 0 && OrderSymbol() == Symbol())
   	{
   		ordersFound++;
   		double profitAchieved = (isLong(OrderType())) ? ND(Bid - OrderOpenPrice()) : ND(OrderOpenPrice() - Ask);
   		bool minProfitAchieved = profitAchieved >= StopToFlatPips*_pnt && (OrderOpenPrice() > 0.0);
   		//Print("  Profit: ", profitAchieved, "    MinProfit: ", minProfitAchieved);
   	   if (minProfitAchieved)
   	   {
            bool stopInProfit = isLong(OrderType()) ? OrderStopLoss() >= OrderOpenPrice() : OrderStopLoss() <= OrderOpenPrice();
            //Print("StopInProfit: ", stopInProfit, "  SL: ", OrderStopLoss(), ",  Open: ", OrderOpenPrice());
   			if (stopInProfit)
   			{
   			   double newSL = isLong(OrderType()) ? Low[1] - _pnt*TrailingStopBufferPips : High[1] + _pnt*TrailingStopBufferPips;
   			   //Print("**** Trailing STF:  newStop: ", ND(Low[1] - _pnt*TrailingStopBufferPips), "   current: ", OrderStopLoss());
   			   bool applyNewStop = isLong(OrderType()) ? ND(newSL) > OrderStopLoss() : ND(newSL) < OrderStopLoss();
   			   if (applyNewStop)
   			   {
   			      trailingCount++;
   			      AdjustStopLoss(OrderTicket(), newSL);
   			   }
   			}
   			else
   			{ // Move stop to flat... first time it has been at minimum profit
   			   //Print("**** Moving STF");
   			   stfCount++;
   			   AdjustStopLoss(OrderTicket(), OrderOpenPrice());
   			}   			   			
   		}
   	}
   }
   
   Print("New bar: ", ordersFound, " orders found, ", stfCount, " STF, ", trailingCount, " stops trailed");		   
}
// -----------------------------------------------------------------------------------------------------------------
// -- MT4 calls these entry point methods --------------------------------------------------------------------------
// -- You probably won't need to touch these at all unless you are doing something advanced ------------------------
// -----------------------------------------------------------------------------------------------------------------
int OnInit()
{
   CalcPointValue();
   return(INIT_SUCCEEDED);
}

// Called by MT4 to uninitialize the EA
void OnDeinit(const int reason) { }

// Called by MT4 once per tick (potentially many times per second).  Be careful about doing things in here every tick.
int t = -1;
void OnTick()
{   
   // *** For sim testing ONLY
   //if (t < 0)
   //{
   //   t = OrderSend(Symbol(), OP_BUY, 1.0, ND(Ask), 0, ND(Ask - _pnt*25.0), 0, "no comment", 123);
   //   //t = OrderSend(Symbol(), OP_SELL, 1.0, ND(Bid), 0, ND(Ask + _pnt*25.0), 0, "no comment", 123);
   //}
   
   if (Time[0] != _lastActionTime)  // time[0] is the time of the frist tick of the current bar
   {
      _lastActionTime = Time[0]; 
      OnNewBar();
   }
}

// -----------------------------------------------------------------------------------------------------------------
// Global variables needed for EA housekeeping ---------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------
double   _pnt;              // Multiplier used to convert to and from Pips and price differences so our input params 
                            // to the EA can be in Pips.  
                            
datetime _lastActionTime;   // This is used to store the Bar Time of the last action the EA did, which allows us to 
                            // call the OnNewBar once per bar
                            
// -----------------------------------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------------------------
// ---- Below this line are global variables and helper functions that are part of the EA template. ---------------- 
// ---- Try not to change them because changing them will make it hard to upgrade you code to new template versions-
// -----------------------------------------------------------------------------------------------------------------

void CalcPointValue()
{
   _pnt = Point() * ((Digits() == 3 || Digits() == 5) ? 10 : 0);
}
       
double ND(double val)
{
   return(NormalizeDouble(val, Digits()));
}

bool isLong(int orderType)
{
   return orderType == OP_BUY || orderType == OP_BUYLIMIT || orderType == OP_BUYSTOP;
}

bool isShort(int orderType)
{
   return orderType == OP_SELL || orderType == OP_SELLLIMIT || orderType == OP_SELLSTOP;
}

void AdjustStopLoss(int ticket, double newStop)
{
   if (!OrderModify(ticket, OrderOpenPrice(), ND(newStop), OrderTakeProfit(), 0))
      Print("Error trailing SL on _ticket ", ticket, " to ", ND(newStop));
}

