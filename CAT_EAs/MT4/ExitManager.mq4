//+------------------------------------------------------------------------+
//|                                          ExitManager.mq4, Version 1.00 |
//|                                         Copyright 2019, John Fairbanks | 
//|                                                      Apiary: ScrubFree |
//|                                                 john@hattrickforex.com |
//|                                                                        |
//|                           https://gitlab.com/jfairbanks/hattrickpublic |
//+------------------------------------------------------------------------+
// 
// This EA attempts to manage existing open orders which have a comment
// that begins with the text *EM* as a signal that the order should be
// managed by the EA.  This EA is tuned for use with a 5 Min chart.
//
// This EA is for use in trending markets to let the trades travel to the
// best possible return without needing to close them early to ensure
// a profit.
//
// It manages exists two ways:
//   1) It trails a stoploss at a long period MA
//   2) It monitors trend direction through a combination of long and short
//      MAs to decide if the trend has actually reversed and not just 
//      experiencing a pullback that the EA should try and hold through.
//
//
//
#property copyright "Copyright 2019, John Fairbanks (john@hattrickforex.com)"
#property link      "https://gitlab.com/jfairbanks/hattrickpublic"
#property version   "1.00"
#property strict

// -----------------------------------------------------------------------------------------------------------------
// EA Settings ------
extern int    LongMAPeriod      = 120; 
extern int    ShortMAPeriod     = 20; 
extern double PipsDiffForClose  = 0.3;

// -----------------------------------------------------------------------------------------------------------------
// EA Logic --------- 
void OnNewBar()
{
   double shortMA = iMA(NULL, 0, ShortMAPeriod, 0, MODE_SMA, PRICE_CLOSE, 1);
   double longMA = iMA(NULL, 0, LongMAPeriod, 0, MODE_SMA, PRICE_CLOSE, 1);
   double prevSig = iMA(NULL, 0, LongMAPeriod, 0, MODE_SMA, PRICE_CLOSE, 2) + iMA(NULL, 0, ShortMAPeriod, 0, MODE_SMA, PRICE_CLOSE, 2);
   double sig = longMA + shortMA;
   double diff = ND(sig - prevSig) / _pnt;   

   for (int i = OrdersTotal() - 1; i >= 0; i--)
   {
      if (OrderSelect(i, SELECT_BY_POS) && OrderCloseTime() == 0 && StringFind(OrderComment(),"*EM*") >= 0)
      {
         // Check the change in our "signal" from the last bar to see if we are making a strong momentum move in the wrong direction
         // and if so, then close the order out.  Sensitivity of this is controlled by the PipsDiffForClose setting
         if ((isLong(OrderType()) && diff < -PipsDiffForClose) || (isShort(OrderType()) && diff > PipsDiffForClose))
         {         
            if (!OrderClose(OrderTicket(), OrderLots(), Ask, 10))
               Print("Error Closing Order #", OrderTicket());
         }
         // If we are a BUY and in an upswing
         else if (isLong(OrderType()) && High[1] > shortMA || isShort(OrderType()) && Low[1] < shortMA)
            AdjustStopLoss(OrderTicket(), longMA);         
      }
   }
}

// -----------------------------------------------------------------------------------------------------------------
// -- MT4 calls these entry point methods --------------------------------------------------------------------------
// -- You probably won't need to touch these at all unless you are doing something advanced ------------------------
// -----------------------------------------------------------------------------------------------------------------
int OnInit()
{
   CalcPointValue();
   return(INIT_SUCCEEDED);
}

// Called by MT4 to uninitialize the EA
void OnDeinit(const int reason) { }

// Called by MT4 once per tick (potentially many times per second).  Be careful about doing things in here every tick.
void OnTick()
{   
   if (Time[0] != _lastActionTime)  // time[0] is the time of the frist tick of the current bar
   {
      _lastActionTime = Time[0]; 
      OnNewBar();
   }
}

// -----------------------------------------------------------------------------------------------------------------
// Global variables needed for EA housekeeping ---------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------
double   _pnt;              // Multiplier used to convert to and from Pips and price differences so our input params 
                            // to the EA can be in Pips.  
                            
datetime _lastActionTime;   // This is used to store the Bar Time of the last action the EA did, which allows us to 
                            // call the OnNewBar once per bar
                            
// -----------------------------------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------------------------
// ---- Below this line are global variables and helper functions that are part of the EA template. ---------------- 
// ---- Try not to change them because changing them will make it hard to upgrade you code to new template versions-
// -----------------------------------------------------------------------------------------------------------------

void CalcPointValue()
{
   _pnt = Point() * ((Digits() == 3 || Digits() == 5) ? 10 : 0);
}
       
double ND(double val)
{
   return(NormalizeDouble(val, Digits()));
}

bool isLong(int orderType)
{
   return orderType == OP_BUY || orderType == OP_BUYLIMIT || orderType == OP_BUYSTOP;
}

bool isShort(int orderType)
{
   return orderType == OP_SELL || orderType == OP_SELLLIMIT || orderType == OP_SELLSTOP;
}

void AdjustStopLoss(int ticket, double newStop)
{
   if (!OrderModify(ticket, OrderOpenPrice(), ND(newStop), OrderTakeProfit(), 0))
      Print("Error trailing SL on _ticket ", ticket, " to ", ND(newStop));
}

