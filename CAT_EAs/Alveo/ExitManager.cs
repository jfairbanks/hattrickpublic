
using System;
using System.ComponentModel;
using System.Windows.Media;
using System.Threading;
using Alveo.Interfaces.UserCode;
using Alveo.UserCode;
using Alveo.Common;
using Alveo.Common.Classes;

namespace Alveo.UserCode
{
    [Serializable]
    [Description("")]
    public class ExitManager : ExpertAdvisorBase
    {
        //+------------------------------------------------------------------------+
        //|                                           ExitManager.cs, Version 1.01 |
        //|                                         Copyright 2019, John Fairbanks | 
        //|                                                      Apiary: ScrubFree |
        //|                                                 john@hattrickforex.com |
        //|                                                                        |
        //|                           https://gitlab.com/jfairbanks/hattrickpublic |
        //+------------------------------------------------------------------------+
        // 
        // This EA attempts to manage existing open orders which have a comment
        // that begins with the text *EM* as a signal that the order should be
        // managed by the EA.  This EA is tuned for use with a 5 Min chart.
        //
        // This EA is for use in trending markets to let the trades travel to the
        // best possible return without needing to close them early to ensure
        // a profit.
        //
        // It manages exists two ways:
        //   1) It trails a stoploss at a long period MA
        //   2) It monitors trend direction through a combination of long and short
        //      MAs to decide if the trend has actually reversed and not just 
        //      experiencing a pullback that the EA should try and hold through.
        //
        // In v 1.01 I have added a Min Pips Profit setting that will prevent the
        // stop adjustments and the momentum exits until at least that amount of pips
        // profit is attained.  This allows trades to be entered early in a move with
        // a larger stop and not just get whipped out.

        #region Properties

        [DisplayName("Period of the long MA")]
        [Description("The long MA is used for used the moving SL and signaling an exit")]
        public int LongMAPeriod { get; set; }

        [DisplayName("Period of the short MA")]
        [Description("The short MA is used to decide if a SL should be moved now, and also for signaling an exit")]
        public int ShortMAPeriod { get; set; }        
        
        [DisplayName("Contrarian move pip max")]
        [Description("Difference in pips for a single bar contrary move of the signal indicator to cause an immediate exit regardless of SL")]
        public double PipsDiffForClose { get; set; }

        [DisplayName("Min pips profit before exits")]
        [Description("Min number of pips profit that must be achieved before the SL starts to adjust and momentum exits will occur")]
        public double MinPipsProfitForSL { get; set; }

        #endregion

        public ExitManager()
        {
        	LongMAPeriod       = 120; 
			ShortMAPeriod      = 20; 
			PipsDiffForClose   = 0.3;
			MinPipsProfitForSL = 10.0;
            
            copyright = "johnfairbanks";
            link = "www.hattrickforex.com";
        	
        }

        private void OnNewBar()
        {
			double shortMA = iMA(null, 0, ShortMAPeriod, 0, MODE_SMA, PRICE_CLOSE, 1);
			double longMA = iMA(null, 0, LongMAPeriod, 0, MODE_SMA, PRICE_CLOSE, 1);
			double prevSig = iMA(null, 0, LongMAPeriod, 0, MODE_SMA, PRICE_CLOSE, 2) + iMA(null, 0, ShortMAPeriod, 0, MODE_SMA, PRICE_CLOSE, 2);
			double sig = longMA + shortMA;
			double diff = ND(sig - prevSig) / _pnt;  
						
			int ordersFound = 0; int minProfitCount = 0;
			for (int i = OrdersTotal() - 1; i >= 0; i--)
			{
				if (OrderSelect(i, SELECT_BY_POS) && OrderCloseTime() == DateTime.MinValue && StringFind(OrderComment(),"*EM*") >= 0)
				{
					ordersFound++;
					bool minProfitAchieved = ( (isLong(OrderType())) ? ND(Bid - OrderOpenPrice()) >= MinPipsProfitForSL*_pnt : ND(OrderOpenPrice() - Ask) >= MinPipsProfitForSL*_pnt ) && (OrderOpenPrice() > 0.0);
					if (minProfitAchieved)
						minProfitCount++;
					
					// Check the change in our "signal" from the last bar to see if we are making a strong momentum move in the wrong direction
					// and if so, then close the order out.  Sensitivity of this is controlled by the PipsDiffForClose setting
					if (((isLong(OrderType()) && diff < -PipsDiffForClose) || (isShort(OrderType()) && diff > PipsDiffForClose)) && minProfitAchieved)
					{
						try
						{						
							OrderClose(OrderTicket(), OrderLots(), Ask, 10);
						}
						catch (Exception ex) { Print("Error closing trade ", OrderTicket(), "\n", ex.ToString()); }
					}
					// If we are a BUY and in an upswing, or a SELL and a downswing
					else if ((isLong(OrderType()) && High[1] > shortMA || isShort(OrderType()) && Low[1] < shortMA) && minProfitAchieved)
						AdjustStopLoss(OrderTicket(), longMA);         
				}
			}
			
			Print("New bar: diff: ", diff, "   Found ", ordersFound, " orders to manage, ", minProfitCount, " at minProfit");		   

        }

        protected override int Init()
        {
            try
            {
                Print("HatTrick - ExitManager v1.0 Init()");
                CalcPointValue();
            }
            catch (Exception ex) { Print(ex.ToString()); }

            return 0;
        }

        protected override int Deinit()
        {
            try
            {
                Print("HatTrick - ExitManager v1.0 DeInit()");
            }
            catch (Exception ex) { Print(ex.ToString()); }

            return 0;
        }

        private SemaphoreSlim _startLock = new SemaphoreSlim(1);
        protected override int Start()
        {
            if (_startLock.Wait(0))
            {
                try
                {
                    // only do stuff at start of new bars
                    if (Time[0] != _lastActionTime)
                    {
                        _lastActionTime = Time[0];
                        OnNewBar();
                    }
                }
                catch (Exception ex) { Print(ex.ToString()); }
                finally { _startLock.Release(); }

            }
            return 0;
        }

        // Multiplier used to convert to and from Pips and price differences so our input params 
        // to the EA can be in Pips.  
        private double _pnt;   
                                                                                                        
        private DateTime _lastActionTime;   // This is used to store the Bar Time of the last action the EA did, which allows us to 
                                            // call the OnNewBar once per bar

        private const string _orderPrefix = "*EM*";

        void CalcPointValue()
        {
            _pnt = Point * ((Digits == 3 || Digits == 5) ? 10 : 0);
        }

        double ND(double val)
        {
            return (NormalizeDouble(val, Digits));
        }

        bool isLong(int orderType)
		{
		   return orderType == OP_BUY || orderType == OP_BUYLIMIT || orderType == OP_BUYSTOP;
		}

		bool isShort(int orderType)
		{
		   return orderType == OP_SELL || orderType == OP_SELLLIMIT || orderType == OP_SELLSTOP;
		}

        void AdjustStopLoss(int ticket, double newStop)
        {
            if (!OrderModify(ticket, OrderOpenPrice(), ND(newStop), OrderTakeProfit(), 0))
                Print("Error trailing SL on _ticket ", ticket, " to ", ND(newStop));
        }


    }
}