
using System;
using System.ComponentModel;
using System.Windows.Media;
using System.Threading;
using Alveo.Interfaces.UserCode;
using Alveo.UserCode;
using Alveo.Common;
using Alveo.Common.Classes;

namespace Alveo.UserCode
{
    [Serializable]
    [Description("This EA implements a trailing stop for use with the Gold Method Ichimoku trading group")]
    public class GoldTrailingStop : ExpertAdvisorBase
    {
        //+----------------------------------------------------------------------------+
        //|                                          GoldTrailingStop.cs, Version 1.00 |
        //|                                             Copyright 2020, John Fairbanks | 
        //|                                                          Apiary: ScrubFree |        
        //|                               https://gitlab.com/jfairbanks/hattrickpublic |
        //|                                                                            |
        //|	This EA implements a trailing stop for use with the Gold Method Ichimoku   | 
        //| trading group:  https://www.tradingview.com/u/AGold54                      |
        //|                                                                            |
        //+----------------------------------------------------------------------------+
        // 

        #region Properties       

        [DisplayName("Profit pips before STF and start trailing")]
        [Description("Number of pips of profit before STF and trailing stop management begins")]
        public double StopToFlatPips { get; set; }

        [DisplayName("Pips above/below last candle when trailing")]
        [Description("Pips of buffer above/below last candle high/low when trailing")]
        public double TrailingStopBufferPips { get; set; }

        #endregion

        public GoldTrailingStop()
        {
        	StopToFlatPips          = 25.0; 
			TrailingStopBufferPips  = 5.0; 
            
            copyright = "This software is for use with the Gold Method Ichimoku trading group";
            link = "www.hattrickforex.com";        	
        }

        private void OnNewBar()
        {
			int ordersFound = 0; int stfCount = 0; int trailingCount = 0;
			for (int i = OrdersTotal() - 1; i >= 0; i--)
			{
				if (OrderSelect(i, SELECT_BY_POS) && OrderCloseTime() == DateTime.MinValue && OrderSymbol() == Symbol())
				{
			   		ordersFound++;
			   		double profitAchieved = (isLong(OrderType())) ? ND(Bid - OrderOpenPrice()) : ND(OrderOpenPrice() - Ask);
			   		bool minProfitAchieved = profitAchieved >= StopToFlatPips*_pnt && (OrderOpenPrice() > 0.0);
			   		//Print("  Profit: ", profitAchieved, "    MinProfit: ", minProfitAchieved);

			   		if (minProfitAchieved)
					{
				        bool stopInProfit = isLong(OrderType()) ? OrderStopLoss() >= OrderOpenPrice() : OrderStopLoss() <= OrderOpenPrice();
				        //Print("StopInProfit: ", stopInProfit, "  SL: ", OrderStopLoss(), ",  Open: ", OrderOpenPrice());
						if (stopInProfit)
						{
			   			   double newSL = isLong(OrderType()) ? Low[1] - _pnt*TrailingStopBufferPips : High[1] + _pnt*TrailingStopBufferPips;
			   			   //Print("**** Trailing STF:  newStop: ", ND(Low[1] - _pnt*TrailingStopBufferPips), "   current: ", OrderStopLoss());
			   			   bool applyNewStop = isLong(OrderType()) ? ND(newSL) > OrderStopLoss() : ND(newSL) < OrderStopLoss();
			   			   if (applyNewStop)
			   			   {
			   			      trailingCount++;
			   			      AdjustStopLoss(OrderTicket(), newSL);
			   			   }
			   			}
			   			else
			   			{ // Move stop to flat... first time it has been at minimum profit
			   			   //Print("**** Moving STF");
			   			   stfCount++;
			   			   AdjustStopLoss(OrderTicket(), OrderOpenPrice());
			   			}   			   			
			   		}
				}
   			}
   
   			Print("New bar: ", ordersFound, " orders found, ", stfCount, " STF, ", trailingCount, " stops trailed");		   
        }

        protected override int Init()
        {
            try
            {
                Print("Gold Trailing Stop - v1.0 Init()");
                CalcPointValue();
            }
            catch (Exception ex) { Print(ex.ToString()); }

            return 0;
        }

        protected override int Deinit()
        {
            try
            {
                Print("Gold Trailing Stop - v1.0 DeInit()");
            }
            catch (Exception ex) { Print(ex.ToString()); }

            return 0;
        }

        private SemaphoreSlim _startLock = new SemaphoreSlim(1);
        protected override int Start()
        {
            if (_startLock.Wait(0))
            {
                try
                {
                    // only do stuff at start of new bars
                    if (Time[0] != _lastActionTime)
                    {
                        _lastActionTime = Time[0];
                        OnNewBar();
                    }
                }
                catch (Exception ex) { Print(ex.ToString()); }
                finally { _startLock.Release(); }

            }
            return 0;
        }

        // Multiplier used to convert to and from Pips and price differences so our input params 
        // to the EA can be in Pips.  
        private double _pnt;   
                                                                                                        
        private DateTime _lastActionTime;   // This is used to store the Bar Time of the last action the EA did, which allows us to 
                                            // call the OnNewBar once per bar

        void CalcPointValue()
        {
            _pnt = Point * ((Digits == 3 || Digits == 5) ? 10 : 0);
        }

        double ND(double val)
        {
            return (NormalizeDouble(val, Digits));
        }

        bool isLong(int orderType)
		{
		   return orderType == OP_BUY || orderType == OP_BUYLIMIT || orderType == OP_BUYSTOP;
		}

		bool isShort(int orderType)
		{
		   return orderType == OP_SELL || orderType == OP_SELLLIMIT || orderType == OP_SELLSTOP;
		}

        void AdjustStopLoss(int ticket, double newStop)
        {
            if (!OrderModify(ticket, OrderOpenPrice(), ND(newStop), OrderTakeProfit(), 0))
                Print("Error trailing SL on _ticket ", ticket, " to ", ND(newStop));
        }


    }
}
