PURPOSE

This repository contains source code and executable strategy testing tools created to assist a person in the
creation of an EA that will operate under the Alveo trading platform of the Apiary Fund, www.apiaryfund.com

AUTHOR

This repositiory is maintained by John Fairbanks (ScrubFree), john@hattrickforex.com 

CONTENTS

    EATemplates     Contains a starting point template for creating and optimizing EAs in MT4,
                    and an Alveo template to port the finished strategy into.  The idea is to
                    allow the template to hide MT4/Alveo implementation differences, and ease
                    the porting into Alveo, leaving in the end working EAs for both systems
                    that perform indentically.

    CAT_EAs         Alveo CAT (Computer Assisted Trading) EAs that are intended to assist a manual trader
                    to trade more effectively


DISCLAIMER

I am making you aware that this Expert Advisor and any other code you download and/or copy from this repositiory
is being provided exclusively for educational and entertainment purposes.  Any source code or software posted here
is provided ‘as is’ solely for informational purposes and is not intended for trading purposes or advice. 
If past performace of EAs is shown you acknowledge that past performance is no guarantee of future results. 
If you choose to use this system on any trading accounts, you are doing so at your own risk, and you are liable
for any risk encountered for any reason including the failure or misperformance of the software.
