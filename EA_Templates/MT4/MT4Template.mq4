//+------------------------------------------------------------------------+
//|                                        MT4EATemplate.mq4, Version 1.00 |
//|                                         Copyright 2019, John Fairbanks | 
//|                                                      Apiary: ScrubFree |
//|                                                 john@hattrickforex.com |
//|                                                                        |
//|                           https://gitlab.com/jfairbanks/hattrickpublic |
//+------------------------------------------------------------------------+
// 
#property copyright "Copyright 2019, John Fairbanks (john@hattrickforex.com)"
#property link      "https://gitlab.com/jfairbanks/hattrickpublic"
#property version   "1.00"
#property strict
//
// The purpose of this template is to provide an easy starting point for writing creating and optimizing 
// an EA in MT4 with the ultimate goal of porting it into the Alveo template code to have a working 
// EA in Alveo.
//
// The goal is to try and keep as much of the template as free of logic as possible except in designated
// areas, which makes it very easy to port only those parts and to know that everything else should
// just work fine, with the template taking the burden of adding in any Alveo specific changes that
// need to be made for things to work.
//
// I also want to have this template serve as example code, so I am releasing this with a very simple
// example EA that does stupid things, but which highlights how you would want to do some things



// -----------------------------------------------------------------------------------------------------------------
// EA Settings -------YOU WILL EDIT THIS SECTION TO ADD ANY SETTINGS FOR YOUR EA------------------------------------
extern double Lots         =  0.1;   // Base position size for a trade

extern int    NumLookbackBars =  3;  // Number of bars we will look back when deciding about placing a trade
                                     // I really only included this because it gives an example of a setting you
                                     // can optimize over with this example EA

extern int    SignalEMAPeriod = 11;  // The period of the EMA used to signal an entry
extern int    TargetSMAPeriod = 80;  // The period of the SMA used to signal an exit

// -----------------------------------------------------------------------------------------------------------------

// --------------------------------------  EVIL GLOBAL VARIABLES  --------------------------------------------------
// If you need global variables for your EA logic be aware that you must get them re-initialzed in OnInit() in 
// case things go down with an order open.  Honestly it's better to avoid them if you can because by being stateless,
// if the system goes down and comes back up you don't have to worry about putting them into the right state.  I know
// a lot of poeple use them, but as long as you are only running once per bar you are better off recalculating stuff
// each time so you don't need to retain the values.
// If after this warning you decide to use them anyway, put the global declarations in this section
//
//int unUsedGlobalVariable = 0
//
// -----------------------------------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------------------------
// EA Logic ---- FOR SIMPLE EAs YOU WILL ONLY USE THIS METHOD-------------------------------------------------------
void OnNewBar()
{
   // The code below is all exaple code and it can be modified or completely gutted to create a simple EA.
   // This example EA is going to be really stupid, but should illustrate some important concepts.

   // Do we have an open order?  This check will also return false if the open order from last pass was closed out
   // from under us by the system through hitting an SL or TP
   if (!OpenOrderExists())
   {
      // No current order so we will look at placing one if our criteria is met      
      double lookbackBarMidPoint = High[NumLookbackBars] - Low[NumLookbackBars];
      double EMASignal = iMA(NULL, 0, SignalEMAPeriod, 0, MODE_EMA, PRICE_CLOSE, 0);   
      double SMATarget = iMA(NULL, 0, TargetSMAPeriod, 0, MODE_SMA, PRICE_CLOSE, 0);
      
      // If we closed in the upper half of the lookback bar and we are over the EMA(8) but under SMA(60), place a buy.  
      // NOTE:  Close[NumLookbackBars] is the close of the lookback bar.  Close[0] is the current price (last trade) in the current bar
      if (Close[NumLookbackBars] >= lookbackBarMidPoint && Close[0] > EMASignal && Close[0] < SMATarget)
      {
         double SL = Low[NumLookbackBars] - 1.0*_pnt; // Low of the lookback bar - 1.0 pips (we multiply by _pnt to convert from 1.0 pips to 0.00010)
         double TP = SMATarget;
         PlaceOrder(OP_BUY, SL, TP);         
      }
      // If we closed in the lower half of the lookback bar and we are under the EMA(8) but over SMA(60), place a sell.  
      else if (Close[NumLookbackBars] < lookbackBarMidPoint && Close[0] < EMASignal && Close[0] > SMATarget)
      {
         double SL = High[NumLookbackBars] + 1.0*_pnt; // High of the lookback bar + 1.0 pips (we multiply by _pnt to convert from 1.0 pips to 0.00010)
         double TP = SMATarget;
         PlaceOrder(OP_SELL, SL, TP);
      }
      else // No trade
         Print("No trade this bar.");            
   }
   else // we have an open order already
   {
      // adjust the TP to follow the target SMA
      double newTP = iMA(NULL, 0, TargetSMAPeriod, 0, MODE_SMA, PRICE_CLOSE, 0);
      AdjustTakeProfit(OrderTicket(), newTP);
   }         
                      
}
// -----------------------------------------------------------------------------------------------------------------
// -- End of code you need to modify for a simple EA
// -----------------------------------------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------------------------------------
// -- MT4 calls these entry point methods --------------------------------------------------------------------------
// -- You probably won't need to touch these at all unless you are doing something advanced ------------------------
// -----------------------------------------------------------------------------------------------------------------
int OnInit()
{
   CalcPointValue();
   FindOpenTickets();
      
   return(INIT_SUCCEEDED);
}

// Called by MT4 to uninitialize the EA
void OnDeinit(const int reason) { }

// Called by MT4 once per tick (potentially many times per second).  Be careful about doing things in here every tick.
void OnTick()
{   
   if (OkToTrade())
   {
      if (Time[0] != _lastActionTime)  // time[0] is the time of the frist tick of the current bar
      {
         _lastActionTime = Time[0]; 
         OnNewBar();
      }
   }
   
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//         EA TEMPLATE CODE BEYOND THIS POINT... DO NOT MODIFY ANYTHING IF YOU INTEND TO UPGRADE YOUR 
//         TEMPLATE TO A FUTURE VERSION RELEASED DOWN THE ROAD!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// -----------------------------------------------------------------------------------------------------------------
// ---- Below this line are global variables and helper functions that are part of the EA template. ---------------- 
// ---- Try not to change them because changing them will make it hard to upgrade you code to new template versions-
// -----------------------------------------------------------------------------------------------------------------

// Global variables needed for EA housekeeping ---------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------
double   _pnt;              // Multiplier used to convert to and from Pips and price differences so our input params 
                            // to the EA can be in Pips.  
                            
int      _tickets[30] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };  // Trade tickets... 
                            // MaxOrder controls # simultaneous orders up to a hard max of 10

int      _maxOrders = 30; // This is a setting for the EA... DO NOT REMOVE THIS SETTING as it is part of the EA
                          // The maximum number of cuncurrent orders for this EA (1-10).  
                          // It should not be > than the size of the _tickets array.
                       
double   _OandaOffsets[30] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                            // Needed to tweak position size of each order to comply with Oanda FIFO rules
                            // I put these in an array in case you wanted to weight the positions differently,
                            // just make sure that each order's position size is unique if your broker requires it
                            // and you plan to trade on a real account... it doesn't matter for testing EAs. 
                            // For testing I leave them the same since Alveo doesn't need to follow FIFO                           
                            
datetime _lastActionTime;   // This is used to store the Bar Time of the last action the EA did, which allows us to 
                            // call the OnNewBar once per bar
                            
int      _magic = 13572468; // Magic number is used to locate trades managed by this EA. The actual value of this
                            // number is meaningless and just needs to be set to something we can search for if the
                            // system restarts with orders still open.

bool     _okToTrade = false; // Is it ok for the system to place a trade                          
// -----------------------------------------------------------------------------------------------------------------

void CalcPointValue()
{
   _pnt = Point() * ((Digits() == 3 || Digits() == 5) ? 10 : 0);
}

bool OpenOrderExists()
{
   // see if _ticket already closed via SL or TP
   bool isOpen = false;
   for (int i=0; i < _maxOrders; i++)
   {
      if (_tickets[i] > 0 && OrderSelect(_tickets[i], SELECT_BY_TICKET) && OrderCloseTime() > 0)  
         _tickets[i] = 0;
      isOpen = isOpen || (_tickets[i] > 0);
   }

   return isOpen;
}

// I use this in the OnInit() call to look up any open orders from the EA when the EA is restarting
void FindOpenTickets()
{
   int idx = 0;
   for (int i = OrdersTotal() - 1; i >= 0; i--)
   {
       if (OrderSelect(i, SELECT_BY_POS) && OrderCloseTime() == 0 && OrderMagicNumber() == _magic)
           _tickets[idx++] = OrderTicket();
   }
}
        
double ND(double val)
{
   return(NormalizeDouble(val, Digits()));
}

bool OkToTrade()
{
   // These hour settings are in GMT
  	bool rightBeforeMaintenance = Hour() == 20 && Minute() > 50;
  	bool duringMaintenance = Hour() == 21;
  	bool duringAUOnlySession = Hour() == 22;
        	
   bool okToTrade = !rightBeforeMaintenance && !duringMaintenance && !duringAUOnlySession;
   if (_okToTrade != okToTrade)
   {
       Print("*** OkToTrade changed: ", okToTrade);
       if (!okToTrade)
           CloseOpenOrders(); // close all orders since it is not currently ok to trade
   }
   _okToTrade = okToTrade;

   return (okToTrade);
}

void PlaceOrder(int orderType, double SL, double TP)
{
   if (_okToTrade)
   {
      if (orderType == OP_SELL)
         MarketOrderSend(Symbol(), OP_SELL, Lots, ND(Bid), ND(SL), ND(TP), "MT4 Template");
      else if (orderType == OP_BUY)
         MarketOrderSend(Symbol(), OP_BUY, Lots, ND(Ask), ND(SL), ND(TP), "MT4 Template");
   }
   else
      Print("PlaceOrder() -- Not OkToTrade - Order not placed.");
}
   
int MarketOrderSend(string symbol, int cmd, double size, double price, double stoploss, double takeprofit, string comment)
{
   int t = 0;
   int idx = -1;
   for (int i = 0; i < _maxOrders; i++)
   {
      if (_tickets[i] <= 0)
      {
         idx = i;
         break;
      }
   }
   if (idx >= 0)
   {
      int allowedSlippagePips = 2*int(_pnt/Point()); // allows a max of 2 pips of slippage between order request and execution
      t = OrderSend(symbol, cmd, size + _OandaOffsets[idx], ND(price), allowedSlippagePips, ND(stoploss), ND(takeprofit), comment, _magic);
      if(t <= 0) 
         Alert("OrderSend Error: ", GetLastError());
      else
         _tickets[idx] = t;
   }
   else
      Print("No order slots available to place new order.");
         
   return(t);         
}

void CloseOpenOrders()
{
   for (int i = 0; i < _maxOrders; i++)
      if (_tickets[i] > 0)
         CloseOpenOrder(_tickets[i]);
}

void CloseOpenOrder(int ticket)
{
   if (OrderSelect(ticket, SELECT_BY_TICKET))
   {
      double price = isLong(OrderType()) ? Ask : Bid;
      if (OrderSelect(ticket, SELECT_BY_TICKET) && OrderClose(ticket, OrderLots(), price, 10))
      {
          for (int i = 0; i < _maxOrders; i++)
          {
              if (_tickets[i] == ticket)
              {
                  _tickets[i] = 0;
                  break;
              }
          }
      }
   }
   else
       Print("Error Closing Order #", ticket);
}

void CloseOpenOrders(int orderType) // closes all orders of a particular type
{
   int idx = 0;
   for (int i = OrdersTotal() - 1; i >= 0; i--)
   {
       if (OrderSelect(i, SELECT_BY_POS) && OrderCloseTime() == 0 && OrderMagicNumber() == _magic && OrderType() == orderType)
       {
           double price = isLong(OrderType()) ? Ask : Bid;
           if (!OrderClose(OrderTicket(), OrderLots(), price, 10))
               Print("Error Closing Order #", OrderTicket());
       }
   }
}

bool isLong(int orderType)
{
   return orderType == OP_BUY || orderType == OP_BUYLIMIT || orderType == OP_BUYSTOP;
}

bool isShort(int orderType)
{
   return orderType == OP_SELL || orderType == OP_SELLLIMIT || orderType == OP_SELLSTOP;
}

void AdjustStopLoss(int ticket, double newStop)
{
   if (!OrderModify(ticket, OrderOpenPrice(), ND(newStop), OrderTakeProfit(), 0))
      Print("Error trailing SL on _ticket ", ticket, " to ", ND(newStop));
}

void AdjustTakeProfit(int ticket, double newTP)
{
   if (!OrderModify(ticket, OrderOpenPrice(), OrderStopLoss(), ND(newTP), 0))
      Print("Error adjusting TP on _ticket ", ticket, " to ", ND(newTP));
}

void AdjustTargets(int ticket, double newStop, double newTP)
{
   if (!OrderModify(ticket, OrderOpenPrice(), ND(newStop), ND(newTP), 0))
      Print("Error adjusting SL / TP on _ticket ", ticket, " to ", ND(newStop), " / ", ND(newTP));
}
