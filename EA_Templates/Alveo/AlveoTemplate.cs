using System;
using System.ComponentModel;
using System.Windows.Media;
using System.Threading;
using Alveo.Interfaces.UserCode;
using Alveo.UserCode;
using Alveo.Common;
using Alveo.Common.Classes;

namespace Alveo.UserCode
{
    [Serializable]
    [Description("An EA Template for Alveo that matches the MT4 EA template")]
    public class AlveoTemplate : ExpertAdvisorBase
    {
        // -----------------------------------------------------------------------------------------------------------------
        // EA Settings -------YOU WILL EDIT THIS SECTION TO ADD ANY SETTINGS FOR YOUR EA------------------------------------

        [DisplayName("Lots")]
        [Description("Position size")]
        public double Lots { get; set; }

        // I really only included this because it gives an example of a setting you can optimize over with this example EA
        [DisplayName("Num Lookback Bars")]
        [Description("Number of bars we will look back when deciding about placing a trade")]
        public int NumLookbackBars { get; set; }

        [DisplayName("Signal EMA Period")]
        [Description("The period of the EMA used to signal an entry")]
        public int SignalEMAPeriod { get; set; }

        [DisplayName("Target SMA Period")]
        [Description("The period of the SMA used to signal an exit")]
        public int TargetSMAPeriod { get; set; }

        public AlveoTemplate()
        {
            Lots = 0.01;
            NumLookbackBars = 10;
            SignalEMAPeriod = 4;
            TargetSMAPeriod = 16;

            MaxOrders = 1; // this setting is part of the template
            copyright = "Copyright 2019, John Fairbanks (john@hattrickforex.com)";
            link = "https://gitlab.com/jfairbanks/hattrickpublic";
        }

        // -----------------------------------------------------------------------------------------------------------------


        // --------------------------------------  EVIL GLOBAL VARIABLES  --------------------------------------------------
        // If you need global variables for your EA logic be aware that you must get them re-initialzed in OnInit() in 
        // case things go down with an order open.  Honestly it's better to avoid them if you can because by being stateless,
        // if the system goes down and comes back up you don't have to worry about putting them into the right state.  I know
        // a lot of poeple use them, but as long as you are only running once per bar you are better off recalculating stuff
        // each time so you don't need to retain the values.
        // If after this warning you decide to use them anyway, put the global declarations in this section
        //
        //int unUsedGlobalVariable = 0
        //
        // -----------------------------------------------------------------------------------------------------------------

        // -----------------------------------------------------------------------------------------------------------------
        // EA Logic ---- FOR SIMPLE EAs YOU WILL ONLY USE THIS METHOD-------------------------------------------------------
        private void OnNewBar()
        {
            // The code below is all exaple code and it can be modified or completely gutted to create a simple EA.
            // This example EA is going to be really stupid, but should illustrate some important concepts.

            // Do we have an open order?  This check will also return false if the open order from last pass was closed out
            // from under us by the system through hitting an SL or TP
            if (!OpenOrderExists())
            {
                // No current order so we will look at placing one if our criteria is met      
                double lookbackBarMidPoint = High[NumLookbackBars] - Low[NumLookbackBars];
                double EMASignal = iMA(null, 0, SignalEMAPeriod, 0, MODE_EMA, PRICE_CLOSE, 0);
                double SMATarget = iMA(null, 0, TargetSMAPeriod, 0, MODE_SMA, PRICE_CLOSE, 0);

                // If we closed in the upper half of the lookback bar and we are over the EMA(8) but under SMA(60), place a buy.  
                // NOTE:  Close[NumLookbackBars] is the close of the lookback bar.  Close[0] is the current price (last trade) in the current bar
                if (Close[NumLookbackBars] >= lookbackBarMidPoint && Close[0] > EMASignal && Close[0] < SMATarget)
                {
                    double SL = Low[NumLookbackBars] - 1.0 * _pnt; // Low of the lookback bar - 1.0 pips (we multiply by _pnt to convert from 1.0 pips to 0.00010)
                    double TP = SMATarget;
                    PlaceOrder(OP_BUY, SL, TP);
                }
                // If we closed in the lower half of the lookback bar and we are under the EMA(8) but over SMA(60), place a sell.  
                else if (Close[NumLookbackBars] < lookbackBarMidPoint && Close[0] < EMASignal && Close[0] > SMATarget)
                {
                    double SL = High[NumLookbackBars] + 1.0 * _pnt; // High of the lookback bar + 1.0 pips (we multiply by _pnt to convert from 1.0 pips to 0.00010)
                    double TP = SMATarget;
                    PlaceOrder(OP_SELL, SL, TP);
                }
                else // No trade
                    Print("No trade this bar.");
            }
            else // we have an open order already
            {
                // adjust the TP to follow the target SMA
                double newTP = iMA(null, 0, TargetSMAPeriod, 0, MODE_SMA, PRICE_CLOSE, 0);
                AdjustTakeProfit(OrderTicket(), newTP);
            }
        }
        // -----------------------------------------------------------------------------------------------------------------
        // -- End of code you need to modify for a simple EA
        // -----------------------------------------------------------------------------------------------------------------


        // -----------------------------------------------------------------------------------------------------------------
        // -- Alveo calls these entry point methods --------------------------------------------------------------------------
        // -- You probably won't need to touch these at all unless you are doing something advanced ------------------------
        // -----------------------------------------------------------------------------------------------------------------

        protected override int Init()
        {
            try
            {
                Print("Alveo Template Init()");
                CalcPointValue();
                FindOpenTickets();
            }
            catch (Exception ex) { Print(ex.ToString()); }

            return 0;
        }

        protected override int Deinit()
        {
            try
            {
                Print("Alveo Template Deinit()");
            }
            catch (Exception ex) { Print(ex.ToString()); }

            return 0;
        }

        private SemaphoreSlim _startLock = new SemaphoreSlim(1);
        protected override int Start()
        {
            if (_startLock.Wait(0))
            {
                try
                {
                    if (OkToTrade())
                    {
                        // only do stuff at start of new bars
                        if (Time[0] != _lastActionTime)
                        {
                            _lastActionTime = Time[0];
                            OnNewBar();
                        }
                    }
                }
                catch (Exception ex) { Print(ex.ToString()); }
                finally { _startLock.Release(); }

            }
            return 0;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //         EA TEMPLATE CODE BEYOND THIS POINT... DO NOT MODIFY ANYTHING IF YOU INTEND TO UPGRADE YOUR 
        //         TEMPLATE TO A FUTURE VERSION RELEASED DOWN THE ROAD!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // -----------------------------------------------------------------------------------------------------------------
        // Global variables needed for EA housekeeping ---------------------------------------------------------------------
        // -----------------------------------------------------------------------------------------------------------------

        // Multiplier used to convert to and from Pips and price differences so our input params to the EA can be in Pips.  
        private double _pnt;

        // The ticket #s for open trades.  This array allows for a max of 10 trades at a time, and can allow a lot less
        private int[] _tickets = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        // DON'T REMOVE THIS SETTING... THIS IS PART OF THE TEMPLATE
        [DisplayName("Max Concurrent Orders")]
        [Description("The maximum number of cuncurrent orders for this EA (1-10).  It should not be > than the size of the _tickets array.")]
        public int MaxOrders { get; set; }

        // This is used to store the Bar Time of the last action the EA did, which allows us to call the OnNewBar once per bar
        private DateTime _lastActionTime;

        private bool _okToTrade = false;

        // Because Alveo does not have the concept of the Magic # (It is there in the call but it does not work), we need
        // a different way to identify our trades if we get reset... using a prefix on the order comment works great for this
        private const string _orderPrefix = "AETemplate";

        private bool OkToTrade()
        {
            bool stuffIsOpen = false;
            switch (DateTime.Now.DayOfWeek)
            {
                case System.DayOfWeek.Sunday:
                    stuffIsOpen = DateTime.Now.Hour >= 18; // 6pm
                    break;

                case System.DayOfWeek.Friday:
                    stuffIsOpen = DateTime.Now.Hour <= 15; // 3pm
                    break;

                default:
                    {
                        bool rightBeforeMaintenance = DateTime.UtcNow.Hour == 20 && DateTime.UtcNow.Minute > 50;
                        bool duringMaintenance = DateTime.UtcNow.Hour == 21;
                        bool duringAUOnlySession = DateTime.UtcNow.Hour == 22;

                        stuffIsOpen = !rightBeforeMaintenance && !duringMaintenance && !duringAUOnlySession;
                    }
                    break;
            }

            bool okToTrade = stuffIsOpen;
            if (_okToTrade != okToTrade)
            {
                Print("HatTrick SaE - OkToTrade changed: ", okToTrade);
                if (!okToTrade)
                    CloseOpenOrders(); // close all orders since it is not currently ok to trade
            }
            _okToTrade = okToTrade;

            return okToTrade;
        }

        void CalcPointValue()
        {
            _pnt = Point * ((Digits == 3 || Digits == 5) ? 10 : 0);
        }

        void FindOpenTickets()
        {
            int idx = 0;
            for (int i = OrdersTotal() - 1; i >= 0; i--)
            {
                if (OrderSelect(i, SELECT_BY_POS) && OrderCloseTime() == 0 && OrderComment().StartsWith(_orderPrefix))
                    _tickets[idx++] = OrderTicket();
            }
        }

        bool OpenOrderExists()
        {
            // see if _ticket already closed via SL or TP
            bool isOpen = false;
            for (int i = 0; i < MaxOrders; i++)
            {
                if (_tickets[i] > 0 && OrderSelect(_tickets[i], SELECT_BY_TICKET) && OrderCloseTime() > 0)
                    _tickets[i] = 0;
                isOpen = isOpen || (_tickets[i] > 0);
            }

            return isOpen;
        }

        double ND(double val)
        {
            return (NormalizeDouble(val, Digits));
        }

        void PlaceOrder(int orderType, double SL, double TP)
        {
            if (orderType == OP_SELL)
                MarketOrderSend(Symbol(), OP_SELL, Lots, ND(Bid), ND(SL), ND(TP), _orderPrefix);
            else if (orderType == OP_BUY)
                MarketOrderSend(Symbol(), OP_BUY, Lots, ND(Ask), ND(SL), ND(TP), _orderPrefix);
        }

        int MarketOrderSend(string symbol, int cmd, double volume, double price, double stoploss, double takeprofit, string comment)
        {
            int t = 0;
            int idx = -1;
            for (int i = 0; i < MaxOrders; i++)
            {
                if (_tickets[i] <= 0)
                {
                    idx = i;
                    break;
                }
            }
            if (idx >= 0)
            {
                t = OrderSend(symbol, cmd, volume, ND(price), 0, ND(stoploss), ND(takeprofit), comment);
                if (t <= 0)
                    Alert("OrderSend Error: ", GetLastError());
                else
                    _tickets[idx] = t;
            }
            else
                Print("No order slots available to place new order.");

            return (t);
        }

        void CloseOpenOrders()
        {
            for (int i = 0; i < MaxOrders; i++)
                CloseOpenOrder(_tickets[i]);
        }
        void CloseOpenOrder(int ticket)
        {
            try
            {
                OrderClose(ticket, Lots, Ask, 10);
                for (int i = 0; i < MaxOrders; i++)
                {
                    if (_tickets[i] == ticket)
                    {
                        _tickets[i] = 0;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Print("Error Closing Order #", ticket);
                Print(ex.ToString());
            }
        }

        void AdjustStopLoss(int ticket, double newStop)
        {
            if (!OrderModify(ticket, OrderOpenPrice(), ND(newStop), OrderTakeProfit(), 0))
                Print("Error trailing SL on _ticket ", ticket, " to ", ND(newStop));
        }

        void AdjustTakeProfit(int ticket, double newTP)
        {
            if (!OrderModify(ticket, OrderOpenPrice(), OrderStopLoss(), ND(newTP), 0))
                Print("Error adjusting TP on _ticket ", ticket, " to ", ND(newTP));
        }

        void AdjustTargets(int ticket, double newStop, double newTP)
        {
            if (!OrderModify(ticket, OrderOpenPrice(), ND(newStop), ND(newTP), 0))
                Print("Error adjusting SL / TP on _ticket ", ticket, " to ", ND(newStop), " / ", ND(newTP));
        }

    }

}
